<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

class SocialController extends Controller
{
    public function redirect($provider)
    {
        return Socialite::driver($provider)->redirect();
    }
 
    public function callback($provider)
    {
 
        // jika user masih login lempar ke home
        if (Auth::check()) {
            return redirect('/home');
        }
 
        $userSocial = Socialite::driver($provider)->stateless()->user();
        $user = User::where(['email' => $userSocial->getEmail()])->first();
        if ($user) {
            Auth::login($user);
            return redirect('/home');
        } else {
            $newUser = User::create([
                'name' => $userSocial->name,
                'email' => $userSocial->email,
                'provider_id'=> $userSocial->getId(),
                'provider'=> $provider,
                'password' => md5($userSocial->token),
            ]);
            Auth::login($newUser);
            return redirect('/home');
        }
    }
}
